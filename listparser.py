class ListParser:
    """This is a class for solving test task
    Firstly you need to call ListParser.merge_hierarchy(input_list)
    The resulting array will be in ListParser.list_result
    """
    def __init__(self):
        self.list_result = []

    def merge_hierarchy(self, list_input) -> list:
        for item_ in list_input:
            if isinstance(item_, list):
                self.merge_hierarchy(item_)
            elif isinstance(item_, int):
                self.list_result.append(item_)
        return self.list_result

if __name__ == '__main__':
    list_input = [1, 2, 3, [4, [5, 6, 7]], 8, [10, [9]], 100500]
    parser = ListParser()
    parser.merge_hierarchy(list_input)
    print(parser.list_result)

